﻿using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace WooliesXchallenge.CustomFilters
{
    public class TokenAuthorizationFilter : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {

            var queryParams = filterContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);
            var token = queryParams.Where(x => x.Key.ToUpper() == "TOKEN").Select(x => x.Value).ToArray();
            if (token != null && token.Length >0)
            {
                if (token[0] == ConfigurationManager.AppSettings["Token"])
                {
                    base.IsAuthorized(filterContext);
                }
                else
                {
                    this.HandleUnauthorizedRequest(filterContext);
                }

            }
            else
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}