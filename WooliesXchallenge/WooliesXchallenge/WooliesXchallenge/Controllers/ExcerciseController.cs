﻿using System.Threading.Tasks;
using System.Web.Http;
using WooliesXchallenge.ChallengeLogics;
using WooliesXchallenge.CustomFilters;

namespace WooliesXchallenge.Controllers
{
    [RoutePrefix("api/exercise")]
    public class ExcerciseController : ApiController
    {
        private readonly IChallengeLogics _challengeLogics;
        public ExcerciseController(IChallengeLogics challengeLogics)
        {
            _challengeLogics = challengeLogics;
        }

        [ExcerciseExceptionFilter]
        [Route("exercise1/user")]
        [HttpGet]
        public IHttpActionResult GetUser()
        {
            var response =  _challengeLogics.GetUserDetails();

            if (response != null)
            {
                return Ok(response);
            }
            return BadRequest("Bad Request");
        }

        [ExcerciseExceptionFilter]
        [TokenAuthorizationFilter]
        [Route("exercise2/sort")]
        [HttpGet]
        public async Task<IHttpActionResult> Sort(string sortOption)
        {
            if (string.IsNullOrEmpty(sortOption))
            {
                return BadRequest("Bad Request");
            }
            var response = await _challengeLogics.SortProduct(sortOption);

            if (response != null)
            {
                return Ok(response);
            }
            return BadRequest("Bad Request");
        }

        [ExcerciseExceptionFilter]
        [TokenAuthorizationFilter]
        [Route("exercise3/trolleyCalculator")]
        [HttpGet]
        public async Task<IHttpActionResult> TrolleyCalculator()
        {
            var response = await _challengeLogics.LowestPriceTrolley();

            if (response != null)
            {
                return Ok(response);
            }
            return BadRequest("Bad Request");

        }
    }
}
