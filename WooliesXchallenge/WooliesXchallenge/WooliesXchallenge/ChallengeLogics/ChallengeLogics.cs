﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using WooliesXchallenge.Models;
using WooliesXResources.Dto;
using WooliesXResources.Resources;

namespace WooliesXchallenge.ChallengeLogics
{
    public class ChallengeLogics : IChallengeLogics
    {
        private readonly IResourceService _resource;

        public ChallengeLogics(IResourceService resource)
        {
            _resource = resource;
        }
        /// <summary>
        /// Exercise1 
        /// </summary>
        /// <returns>Name and Token to access api endpoints for exercise2 and exercise3 </returns> 
        public UserModel GetUserDetails()
        {
            UserModel user = new UserModel
            {
                Name = "WooliesXuser",
                Token = ConfigurationManager.AppSettings["Token"]
            };
            return user;
        }
        /// <summary>
        /// Exercise2
        /// </summary>
        /// <returns>List of Sorted products based on sortOption</returns>                
        public async Task<List<Product>> SortProduct(string sortOption)
        {
            var products = await _resource.GetProductAsync();
            if (products?.Count > 0)
            {
                switch (sortOption.ToUpper())
                {
                    case "LOW":
                        return products.OrderBy(p => p.Price).ToList();
                    case "HIGH":
                        return products.OrderByDescending(p => p.Price).ToList();
                    case "ASCENDING":
                        return products.OrderBy(p => p.Name).ToList();
                    case "DESCENDING":
                        return products.OrderByDescending(p => p.Name).ToList();
                    case "RECOMMENDED":
                        await ProductsWithSoldQuantity(products);
                        return products.OrderByDescending(p => p.Quantity).ToList();
                    default:
                        return products;
                }
            }

            return products;
        }
        /// <summary>
        /// Helps calculate most popular/recommended product
        /// </summary>
        /// <returns>List of products with the total quantities sold according to Shopper History</returns> 
        public async Task<List<Product>> ProductsWithSoldQuantity(List<Product> products)
        {
            var shopperHistory = await _resource.GetShopperHistory();
            foreach (var trolley in shopperHistory)
            {
                foreach (var product in trolley.Products)
                {
                    products
                        .Where(x => x.Name == product.Name)
                        .ToList()
                        .ForEach(x => { x.Quantity = x.Quantity + product.Quantity; });
                }
            }

            return products;

        }

        /// <summary>
        /// Exercise3
        /// </summary>
        /// <returns>The minimum priced shopping cart from Shopper History </returns> 
        public async Task<TrolleyPriceModel> LowestPriceTrolley()
        {
            var shopperHistory = await _resource.GetShopperHistory();
            var trolleyPrices = new List<TrolleyPriceModel>();
            decimal trolleyPrice = (decimal)0.00;
            foreach (var trolley in shopperHistory)
            {
                foreach (var p in trolley.Products)
                {
                    trolleyPrice = trolleyPrice + p.Quantity * p.Price;
                }
                trolleyPrices.Add(new TrolleyPriceModel { TrolleyPrice = trolleyPrice });
            }

            return trolleyPrices.OrderBy(x => x.TrolleyPrice).ToList().FirstOrDefault();
        }
    }
}

