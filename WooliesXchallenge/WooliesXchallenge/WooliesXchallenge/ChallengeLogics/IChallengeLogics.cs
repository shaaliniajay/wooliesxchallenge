﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesXchallenge.Models;
using WooliesXResources.Dto;

namespace WooliesXchallenge.ChallengeLogics
{
    public interface IChallengeLogics
    {
        UserModel GetUserDetails();
        Task<List<Product>> SortProduct(string sortOption);
        Task<TrolleyPriceModel> LowestPriceTrolley();
       
    }
}