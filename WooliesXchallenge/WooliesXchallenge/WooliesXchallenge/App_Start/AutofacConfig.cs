﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using WooliesXchallenge.ChallengeLogics;
using WooliesXchallenge.Controllers;
using WooliesXResources.Client;
using WooliesXResources.Resources;

namespace WooliesXchallenge.App_Start
{
    public class AutofacConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<ResourceService>().As<IResourceService>().InstancePerRequest();
            builder.RegisterType<HttpClientService>().As<IHttpClientService>().InstancePerRequest();
            builder.RegisterType<ChallengeLogics.ChallengeLogics>().As<IChallengeLogics>().InstancePerRequest();

            Container = builder.Build();

            return Container;
        }
    }
}