﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WooliesXchallenge.ChallengeLogics;
using WooliesXchallenge.Controllers;
using WooliesXchallenge.Models;
using WooliesXResources.Dto;

namespace WooliesXchallenge.Tests.Controllers
{
    [TestClass]
    public class ExcerciseContrrollerTest
    {
        [TestMethod]
        public void User_RetrieveUserNameAndToken()
        {
            var userModel = new UserModel
            {
                Name = "WooliesXuser",
                Token = "2faf784b-bec4-4ff6-8ac4-accfc66654fb"
            };

            var challengeServiceMock = new Mock<IChallengeLogics>();
            challengeServiceMock.Setup(service => service.GetUserDetails()).Returns(userModel);

            ExcerciseController controller = new ExcerciseController(challengeServiceMock.Object);

            var res = controller.GetUser() as OkNegotiatedContentResult<UserModel>;

            Assert.AreNotEqual(null, res);

            Assert.AreEqual(res.Content.Name, "WooliesXuser");
            Assert.AreEqual(res.Content.Token, "2faf784b-bec4-4ff6-8ac4-accfc66654fb");
        }

        [TestMethod]
        public async Task Sort_OrderByProductByPrice()
        {
            var prdouctList = new List<Product>();
            prdouctList.Add(new Product()
            {
                Name = "Product A",
                Quantity = 1,
                Price = 2
            });

            prdouctList.Add(new Product()
            {
                Name = "Product B",
                Quantity = 2,
                Price = 4
            });
            var challengeServiceMock = new Mock<IChallengeLogics>();
            challengeServiceMock.Setup(service => service.SortProduct("low")).Returns(Task.FromResult(prdouctList));

            ExcerciseController controller = new ExcerciseController(challengeServiceMock.Object);

            var res = await controller.Sort("low") as OkNegotiatedContentResult<List<Product>>;

            Assert.AreNotEqual(null, res);

            Assert.AreEqual(res.Content[0].Name, "Product A");
            Assert.AreEqual(res.Content[0].Price, 2);
            Assert.AreEqual(res.Content[1].Name, "Product B");
        }

        [TestMethod]
        public async Task TrolleyCalculaor_MinimumCartPrice()
        {
            var minTrolleyPrice = new TrolleyPriceModel();
            minTrolleyPrice.TrolleyPrice = 5;
      
            var challengeServiceMock = new Mock<IChallengeLogics>();
            challengeServiceMock.Setup(service => service.LowestPriceTrolley()).Returns(Task.FromResult(minTrolleyPrice));

            ExcerciseController controller = new ExcerciseController(challengeServiceMock.Object);

            var res = await controller.TrolleyCalculator() as OkNegotiatedContentResult<TrolleyPriceModel>;

            Assert.AreNotEqual(null, res);

            Assert.AreEqual(res.Content.TrolleyPrice, 5);
            Assert.AreNotEqual(res.Content.TrolleyPrice, -1);
        }
    }
}
