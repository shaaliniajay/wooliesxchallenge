﻿using System.Collections.Generic;

namespace WooliesXResources.Dto
{
    public class ShopperHistory
    {
        public string CustomerId { get; set; }
        public List<Product> Products { get; set; }
    }
}
