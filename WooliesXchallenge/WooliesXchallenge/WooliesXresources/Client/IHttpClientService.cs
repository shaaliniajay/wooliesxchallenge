﻿using System.Net.Http;

namespace WooliesXResources.Client
{
    public interface IHttpClientService
    {
        HttpClient Client { get; }
    }
}
