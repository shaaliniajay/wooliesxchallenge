﻿using System.Net.Http;

namespace WooliesXResources.Client
{
    public class HttpClientService : IHttpClientService
    {
        private  HttpClient client = new HttpClient();
        public HttpClient Client
        {
            get
            {
                return client;
            }
        }

    }
}
