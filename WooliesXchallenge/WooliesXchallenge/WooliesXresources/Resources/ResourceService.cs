﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WooliesXResources.Client;
using WooliesXResources.Dto;

namespace WooliesXResources.Resources
{
    public class ResourceService : IResourceService,IDisposable
    {
        private string _baseUri = "http://dev-wooliesx-recruitment.azurewebsites.net/";
        private readonly IHttpClientService _httpClient;
        private bool _disposed;

        public ResourceService(IHttpClientService httpClientService)
        {
            this._httpClient = httpClientService;
        }

        /// <summary>
        /// Gets products from wooliesv endpoint
        /// </summary>
        /// <returns>List of products</returns>
        public async Task<List<Product>> GetProductAsync()
        {

            var result = await _httpClient.Client.GetAsync(_baseUri + "api/resource/products?token=94e9dba0-136b-41fa-bbd8-2c35051e666d");
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var contents = result.Content.ReadAsStringAsync().Result;
                List<Product> products = JsonConvert.DeserializeObject<List<Product>>(contents);
                return products;
            }
            return null;
        }

        /// <summary>
        /// Gets shopper history from wooliesv endpoint
        /// </summary>
        /// <returns>List of shopperHistory</returns>
        public async Task<List<ShopperHistory>> GetShopperHistory()
        {
            var result = await _httpClient.Client.GetAsync(_baseUri + "api/resource/shopperHistory?token=94e9dba0-136b-41fa-bbd8-2c35051e666d");
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var contents = result.Content.ReadAsStringAsync().Result;
                List<ShopperHistory> shopperHistorys = JsonConvert.DeserializeObject<List<ShopperHistory>>(contents);
                return shopperHistorys;
            }
            return null;

        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _baseUri = string.Empty;
                    _httpClient.Client.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
