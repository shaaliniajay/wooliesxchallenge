﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesXResources.Dto;

namespace WooliesXResources.Resources
{
    public interface IResourceService
    {
        Task<List<Product>> GetProductAsync();
        Task<List<ShopperHistory>> GetShopperHistory();
    }
}
